---
title: Academic publications
layout: docs
permalink: /docs/publications/
---

* *Trusting Trust - Reflections on Trusting Trust* (1984) — Ken Thompson. ([PDF](https://www.ece.cmu.edu/~ganger/712.fall02/papers/p761-thompson.pdf))

* *Fully Countering Trusting Trust through Diverse Double-Compiling* (2005/2009) — David A. Wheeler ([PDF](https://dwheeler.com/trusting-trust/dissertation/wheeler-trusting-trust-ddc.pdf), [...](https://dwheeler.com/trusting-trust/))

* *Functional Package Management with Guix* (2013) — Ludovic Courtès. [[...](https://hal.inria.fr/hal-00824004/en)]

* *Reproducible and User-Controlled Software Environments in HPC with Guix* (2015) — Ludovic Courtès, Ricardo Wurmus [[...](https://hal.inria.fr/hal-01161771/en)]

* *Automated Localization for Unreproducible Builds* (2018) — Zhilei Ren, He Jiang, Jifeng Xuan, Zijiang Yang. ([PDF](https://arxiv.org/pdf/1803.06766.pdf))

* *Transparent, Provenance-assured, and Secure Software-as-a-Service* (2019) - Nachiket Tapas, Francesco Longo, Giovanni Merlino and Antonio Puliafito. ([Link](https://ieeexplore.ieee.org/document/8935014))

* *in-toto: Providing farm-to-table guarantees for bits and bytes* (2019) — Santiago Torres-Arias, New York University; Hammad Afzali, New Jersey Institute of Technology; Trishank Karthik Kuppusamy, Datadog; Reza Curtmola, New Jersey Institute of Technology; Justin Cappos, New York University. ([PDF](https://www.usenix.org/system/files/sec19-torres-arias.pdf))

* *Backstabber's Knife Collection: A Review of Open Source Software Supply Chain Attacks* (2020) — Marc Ohm, Henrik Plate, Arnold Sykosch, Michael Meier. ([PDF](https://arxiv.org/pdf/2005.09535.pdf))

* *Reproducible Containers* (2020) — Navarro Leija, Omar S. and Shiptoski, Kelly and Scott, Ryan G. and Wang, Baojun and Renner, Nicholas and Newton, Ryan R. and Devietti, Joseph. ([...](https://dl.acm.org/doi/10.1145/3373376.3378519))

* *Towards detection of software supply chain attacks by forensic artifacts* (2020) — Marc Ohm, Arnold Sykosch, Michael Meier. ([Link](https://dl.acm.org/doi/10.1145/3407023.3409183))

* *Reproducible builds: Increasing the integrity of software supply chains.* (2021) — Chris Lamb & Stefano Zacchiroli. ([Link](https://ieeexplore.ieee.org/document/9403390/))

* *An Experience Report on Producing Verifiable Builds for Large-Scale Commercial Systems* (2021) - Yong Shi, Mingzhi Wen, Filipe Roseiro Cogo, Boyuan Chen and Zhen Ming Jiang. ([Link](https://ieeexplore.ieee.org/document/9465650))

* *Automated Patching for Unreproducible Builds* (2022) - Zhilei Ren, Shiwei Sun, Jifeng Xuan, Xiaochen Li, and Jiang Hi. ([Link](https://dl.acm.org/doi/10.1145/3510003.3510102))

* *On business adoption and use of reproducible builds for open and closed source software* (2022) — Simon Butler, Jonas Gamalielsson, Björn Lundell, Christoffer Brax, Anders Mattsson, Tomas Gustavsson, Jonas Feist, Bengt Kvarnström & Erik Lönroth. ([Link](https://link.springer.com/article/10.1007/s11219-022-09607-z))

* *Top Five Challenges in Software Supply Chain Security: Observations From 30 Industry and Government Organizations* (2022) William Enck and Laurie Williams. ([Link](https://ieeexplore.ieee.org/document/9740718))

* *It’s like flossing your teeth: On the Importance and Challenges of Reproducible Builds for Software Supply Chain Security* (2023) Marcel Fourné, Dominik Wermke, William Enck, Sascha Fahl, Yasemin Acar. ([PDF](https://marcelfourne.de/fourne-reproducible-builds-2023.pdf), [link](https://doi.ieeecomputersociety.org/10.1109/SP46215.2023.10179320))
