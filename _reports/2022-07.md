---
layout: report
year: "2022"
month: "07"
title: "Reproducible Builds in July 2022"
draft: false
date: 2022-08-04 15:35:26
---

[![]({{ "/images/reports/2022-07/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

Welcome to the **July 2022** report from the [Reproducible Builds](https://reproducible-builds.org) project!

In our reports we attempt to outline the most relevant things that have been going on in the past month. As a brief introduction, the reproducible builds effort is concerned with ensuring no flaws have been introduced during this compilation process by promising identical results are always generated from a given source, thus allowing multiple third-parties to come to a consensus on whether a build was compromised. As ever, if you are interested in contributing to the project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

<br>

#### Reproducible Builds summit 2022

Despite several delays, we are pleased to announce that registration is open for our <a href="{{ "/events/venice2022/" | relative_url }}">in-person summit this year</a>:

<big><center>November 1st → November 3rd</center></big><br>

The event will happen in **Venice (Italy)**. We intend to pick a venue reachable via the train station and an international airport. However, the precise venue will depend on the number of attendees.

**Please see the [announcement email](https://lists.reproducible-builds.org/pipermail/rb-general/2022-July/002666.html) for information about how to register.**

<br>

#### [Is reproducibility practical?](https://hpc.guix.info/blog/2022/07/is-reproducibility-practical/)

[![]({{ "/images/reports/2022-07/guix.png#right" | relative_url }})](https://hpc.guix.info/blog/2022/07/is-reproducibility-practical/)

Ludovic Courtès published an informative blog post this month asking the important question: [Is reproducibility practical?](https://hpc.guix.info/blog/2022/07/is-reproducibility-practical/):

> Our attention was recently caught by a nice [slide deck on the methods and tools for reproducible research in the R programming language](https://web.archive.org/web/20220620120430/https://umr-astre.pages.mia.inra.fr/presentations/reproducible-research-in-r/#/principles-of-reproducible-research). Among those, the talk mentions Guix, stating that it is "for professional, sensitive applications that require ultimate reproducibility", which is "probably a bit overkill for Reproducible Research". While we were flattered to see Guix suggested as good tool for reproducibility, the very notion that there’s a kind of "reproducibility" that is "ultimate" and, essentially, impractical, is something that left us wondering: **What kind of reproducibility do scientists need, if not the "ultimate" kind? Is "reproducibility" practical at all, or is it more of a horizon?**

The post goes on to outlines the concept of reproducibility, situating examples within the context of the [GNU Guix](https://www.gnu.org/software/guix/) operating system.

<br>

#### [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2022-07/diffoscope.png#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb prepared and uploaded versions `218`, `219` and `220` to Debian, as well as made the following changes:

* New features:

    * Support [Haskell 9.x](https://www.haskell.org/ghc/blog/20210204-ghc-9.0.1-released.html) series files. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/bd6c5d42)]

* Bug fixes:

    * Fix a regression introduced in version 207 where *diffoscope* would crash if one directory contained a directory that wasn't in the other. Thanks to Alderico Gallo for the testcase. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/b6a4190d)]
    * Don't traceback if we encounter an invalid Unicode character in Haskell versioning headers. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7b0cff62)]

* Output improvements:

    * Improve output of [Markdown](https://en.wikipedia.org/wiki/Markdown) and [reStructuredText](https://en.wikipedia.org/wiki/ReStructuredText) to use code blocks with highlighting. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/671283e0)]

* Codebase improvements:

    * Space out a file a little. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/644682bb)]
    * Update various copyright years. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a3959c18)]

<br>

#### Mailing list

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month:

* Roland Clobus posted his [*Eleventh status update about reproducible [Debian] live-build ISO images*](ttps://lists.reproducible-builds.org/pipermail/rb-general/2022-July/002665.html), noting — amongst many other things! — that "all major desktops build reproducibly with *bullseye*, *bookworm* and *sid*."

* Santiago Torres-Arias [announced a Call for Papers (CfP)](https://lists.reproducible-builds.org/pipermail/rb-general/2022-July/002651.html) for a new [SCORED](https://scored.dev/) conference, an "academic workshop around software supply chain security". As Santiago highlights, this new conference "invites reviewers from industry, open source, governement and academia to review the papers [and] I think that this is super important to tackle the supply chain security task".

<br>

#### Upstream patches

The Reproducible Builds project attempts to fix as many currently-unreproducible packages as possible. This month, however, we submitted the following patches:

* Bernhard M. Wiedemann

    * [openSUSE monthly report](https://rb.zq1.de/compare.factory-20220731/report-202207.txt)
    * [`acarsdec`](https://build.opensuse.org/request/show/990082) (embeds CPU info with `march=native`)
    * [`casacore`](https://build.opensuse.org/request/show/990086) (embeds CPU info with `march=native`)
    * [`kubernetes`](https://github.com/kubernetes/kubernetes/issues/110928) (uses random name of temporary directory)
    * [`setuptools/python-brotlicffi`](http://bugzilla.suse.com/show_bug.cgi?id=1201127) (toolchain, filesys/readdir)
    * [`sysstat`](https://github.com/sysstat/sysstat/pull/330) (FTBFS in 'single CPU' mode)
    * [`sundials`](https://bugzilla.opensuse.org/show_bug.cgi?id=1201273) (FTBFS in 'single CPU' mode)
    * [`nim`](https://bugzilla.opensuse.org/show_bug.cgi?id=1201371) (FTBFS in 'single CPU' mode)
    * [`doxygen/libzypp`](https://bugzilla.opensuse.org/show_bug.cgi?id=1201579) (toolchain readdir)
    * [`python-pyquil`](https://bugzilla.opensuse.org/show_bug.cgi?id=1201567) (build failure)
    * [`openssl-1_0_0`](https://bugzilla.opensuse.org/show_bug.cgi?id=1201627) (build failure)
    * [`jsonrpc-glib`](https://gitlab.gnome.org/GNOME/jsonrpc-glib/-/issues/2) (FTBFS in 'single CPU' mode)
    * [`slurm`](https://build.opensuse.org/request/show/990637) (Link-Time Optimisation and `.tar` issues)
    * [`wasi-libc`](https://github.com/WebAssembly/wasi-libc/pull/313) (sort the output from `find`)

* Chris Lamb:

    * [#1015245](https://bugs.debian.org/1015245) filed against [`libshumate`](https://tracker.debian.org/pkg/libshumate).
    * [#1015246](https://bugs.debian.org/1015246) filed against [`zeal`](https://tracker.debian.org/pkg/zeal).
    * [#1016186](https://bugs.debian.org/1016186) filed against [`gappa`](https://tracker.debian.org/pkg/gappa).

* Philip Rinn:

    * [#1014877](https://bugs.debian.org/1014877) filed against [`cxref`](https://tracker.debian.org/pkg/cxref).

* Vagrant Cascadian:

    * [#1014426](https://bugs.debian.org/1014426) filed against [`cldump`](https://tracker.debian.org/pkg/cldump).
    * [#1014428](https://bugs.debian.org/1014428) filed against [`xmacro`](https://tracker.debian.org/pkg/xmacro).
    * [#1014559](https://bugs.debian.org/1014559) filed against [`libloki`](https://tracker.debian.org/pkg/libloki).
    * [#1014560](https://bugs.debian.org/1014560) filed against [`ygl`](https://tracker.debian.org/pkg/ygl).
    * [#1014561](https://bugs.debian.org/1014561) filed against [`clp`](https://tracker.debian.org/pkg/clp).
    * [#1014564](https://bugs.debian.org/1014564) filed against [`tvtime`](https://tracker.debian.org/pkg/tvtime).
    * [#1014789](https://bugs.debian.org/1014789) filed against [`rdiff-backup-fs`](https://tracker.debian.org/pkg/rdiff-backup-fs).

<br>

#### [*Reprotest*](https://tracker.debian.org/pkg/reprotest)

[*reprotest*](https://tracker.debian.org/pkg/reprotest) is the Reproducible Builds project's end-user tool to build the same source code twice in widely and deliberate different environments, and checking whether the binaries produced by the builds have any differences. This month, the following changes were made:

* Holger Levsen:

    * Uploaded version `0.7.21` to Debian *unstable* as well as mark `0.7.22` development in the repository&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/eecca82)].
    * Make *diffoscope* dependency unversioned as the required version is met even in Debian *buster*.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/29d61a6)]
    * Revert an accidentally committed hunk.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/d7d6b41)]

* Mattia Rizzolo:

    * Apply a patch from Nick Rosbrook to not force the tests to run only against Python 3.9.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/c6edaf8)]
    * Run the tests through `pybuild` in order to run them against all supported Python 3.x versions.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/f236b9e)]
    * Fix a deprecation warning in the `setup.cfg` file.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/988c79a)]
    * Close a new Debian bug.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/0d23993)]

<br>

### Reproducible builds website

[![]({{ "/images/reports/2022-07/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

A number of changes were made to the Reproducible Builds website and documentation this month, including:

* Arnout Engelen:

    * Add a link to recent [*May Contain Hackers* 2022](https://mch2022.org/) conference talk slides.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/41ea9ffb)]

* Chris Lamb:

    * Correct some grammar.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/3c2186fc)]

* Holger Levsen:

    * Add talk from FOSDEM 2015 presented by Holger and Lunar.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/df1c5a98)]
    * Show date of presentations if we have them.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/7a8fee6b)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/6fc7988a)]
    * Add my presentation from DebConf22&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/c1bb2159)] and from Debian Reunion Hamburg 2022&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/87860264)].
    * Add *dhole* to the speakers of the DebConf15 talk.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/2778dc30)]
    * Add *raboof*'s talk "Reproducible Builds for Trustworthy Binaries" from [*May Contain Hackers*](https://mch2022.org/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/1983a3e1)]
    * Drop some Debian-related suggested ideas which are not really relevant anymore.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/0136a46c)]
    * Add a link to list of packages with patches ready to be NMUed.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/52316442)]

* Mattia Rizzolo:

    * Add information about our upcoming event in Venice.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/cf2163c1)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/18a5a3e4)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/be3b6ebe)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/516092ac)]

<br>

#### Testing framework

[![]({{ "/images/reports/2022-07/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project runs a significant testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org), to check packages and other artifacts for reproducibility. This month, Holger Levsen made the following changes:

* Debian-related changes:

    * Create graphs displaying existing `.buildinfo `files per each Debian suite/arch.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/10be55df)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/15892ba3)]
    * Fix a typo in the Debian dashboard.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3ee1123a)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/bbe6bc48)]
    * Fix some issues in the `pkg-r` package set definition.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6e3b5b88)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6b08429c)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b55b7d35)]
    * Improve the "builtin-pho" HTML output.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/de550cfb)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/53f21e87)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7e050763)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/aca0fa4a)]
    * Temporarily disable all live builds as our snapshot mirror is offline.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/744c8f7b)]

* Automated node health checks:

    * Detect `dpkg` failures.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/50300712)]
    * Detect files with bad UNIX permissions.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/666abffa)]
    * Relax a regular expression in order to detect Debian Live image build failures.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4df1204e)]

* Misc changes:

    * Test that FreeBSD virtual machine has been updated to version 13.1.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b7cd4a28)]
    * Add a reminder about powercycling the `armhf`-architecture `mst0X` node.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/bf75cc7a)]
    * Fix a number of typos.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b7ed982b)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ca7d37d2)]
    * Update documentation.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/50062763)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7b7cca11)]
    * Fix [Munin](https://munin-monitoring.org/) monitoring configuration for some nodes.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e1a028be)]
    * Fix the static IP address for a node.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4baaaf56)]

In addition, Vagrant Cascadian updated host keys for the `cbxi4pro0` and `wbq0` nodes&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/80fcce42)] and, finally, node maintenance was also performed by Mattia Rizzolo [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ddc3434a)] and Holger Levsen [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e48bf2e7)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6d98206f)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a9fbc31c)].

<br>

#### Contact

As ever, if you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
