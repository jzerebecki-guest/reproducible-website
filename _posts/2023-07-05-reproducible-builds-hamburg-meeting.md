---
layout: post
title:  "Reproducible Builds Summit 2023 in Hamburg"
date:   2023-07-05
categories: org
---

We are glad to announce the upcoming *Reproducible Builds Summit*, set to take place from **October 31st to November 2nd, 2023**, in the vibrant city of **Hamburg, Germany**.
{: .lead}

This year, we are thrilled to host the seventh edition of this exciting event following the success of previous summits in various iconic locations around the world, including [Venice]({{ "/events/venice2022/" | relative_url }}) (2022), [Marrakesh]({{ "/events/Marrakesh2019/" | relative_url }}) (2019), [Paris]({{ "/events/paris2018/" | relative_url }}) (2018), [Berlin]({{ "/events/berlin2017/" | relative_url }}) (2017), [Berlin]({{ "/events/berlin2016/" | relative_url }}) (2016) [Athens]({{ "/events/athens2015/" | relative_url }}) (2015).

If you're excited about joining us this year, please make sure to read [the event page which has more details about the event and location]({{ "/events/hamburg2023" | relative_url }}). As in previous years, we will be sending invitations to all those who attended our previous summit events or expressed interest to do so. However also without receiving such a personal invitation please do [email the organizers](mailto:2023-summit-team@lists.reproducible-builds.org) and we will find a way to accommodate you.

### About the event

The Reproducible Builds Summit is a unique gathering that brings together attendees from diverse projects, united by a shared vision of advancing the Reproducible Builds effort. During this enriching event, participants will have the opportunity to engage in discussions, establish connections and exchange ideas to drive progress in this vital field. Our aim is to create an inclusive space that fosters collaboration, innovation and problem-solving.

With your help, we will bring this space (and several other inside areas) into life:

 <img src="{{ "/images/hamburg2023/faux_outdoor.jpg" | relative_url }}" width="640px" /><br>
 <em class="small">The outside area at dock-europe (source: dock-europe.net)</em>


### Schedule

Although the exact content of the meeting will be shaped by the participants, the main goals will include:

* Update & exchange about the status of reproducible builds in various projects.
* Improve collaboration both between and inside projects.
* Expand the scope and reach of reproducible builds to more projects.
* Work together and hack on solutions.
* Establish space for more strategic and long-term thinking than is possible in virtual channels.
* Brainstorm designs on tools enabling users to get the most benefits from reproducible builds.
* Discuss how reproducible builds will be usable and meaningful to users and developers alike.

Logs and minutes will be published after the meeting.

### Location & date

* **October 31st to November 2nd, 2023**
* [Dock Europe](https://dock-europe.net/), Zeiseweg 9, 22765, Hamburg, Germany.

### Registration instructions

Please [reach out]({{ "/events/hamburg2023" | relative_url }}) if you'd like to participate in hopefully interesting, inspiring and intense technical sessions about reproducible builds and beyond!

We look forward to what we anticipate to be an extraordinary event!
